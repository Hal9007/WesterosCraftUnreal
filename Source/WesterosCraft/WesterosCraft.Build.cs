// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

using UnrealBuildTool;
using System.IO;

public class WesterosCraft : ModuleRules
{
	public WesterosCraft(TargetInfo Target)
	{
        MinFilesUsingPrecompiledHeaderOverride = 1;
        bFasterWithoutUnity = true;

        PublicDependencyModuleNames.AddRange(new string[] {"Core", "CoreUObject", "Engine", "InputCore"});
	}
}
