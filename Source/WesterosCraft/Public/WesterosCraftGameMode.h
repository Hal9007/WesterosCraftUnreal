// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once
#include "GameFramework/GameMode.h"
#include "WesterosCraftGameMode.generated.h"

UCLASS(minimalapi)
class AWesterosCraftGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AWesterosCraftGameMode(const FObjectInitializer& ObjectInitializer);
};