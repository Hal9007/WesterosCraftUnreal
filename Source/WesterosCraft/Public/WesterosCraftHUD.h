// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved. 

#pragma once 

#include "WesterosCraftHUD.generated.h"

UCLASS()
class AWesterosCraftHUD : public AHUD
{
	GENERATED_UCLASS_BODY()

public:

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	UTexture2D* CrosshairTex;

};

