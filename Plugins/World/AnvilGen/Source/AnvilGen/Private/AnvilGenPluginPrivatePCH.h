// Copyright 2015, WesterosCraft <http://www.westeroscraft.com>. All Rights Reserved.

#pragma once;

#include <algorithm>
#include <iterator>
#include <string>

#include "CoreUObject.h"
#include "Engine.h"
#include "IAnvilGenPlugin.h"
#include "mcmap.h"